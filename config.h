#define CMDLENGTH 60
#define DELIMITER " | "
#define CLICKABLE_BLOCKS

const Block blocks[] = {
	BLOCK("/home/teri/wm/dwmblocks-async/scripts/wifi",  5,    22),
	BLOCK("/home/teri/wm/dwmblocks-async/scripts/brightness",  5,    22),
	BLOCK("/home/teri/wm/dwmblocks-async/scripts/volume",  1,    22),
	BLOCK("/home/teri/wm/dwmblocks-async/scripts/battery", 30,    23),
	BLOCK("/home/teri/wm/dwmblocks-async/scripts/date",    15,    24)
};
